import React, { Component } from 'react';

import Block from './block';
import Navbar from './navbar';
import Footer from './footer';
import AddForm from './addForm';

class Store extends Component {

    render() { 
        const {items} = this.props;
        return ( 
            <>
                <Navbar />
                <div className='main'>
                    {items.map((item) => 
                    <Block key = {item.id}  item={item}/>
                )}
                </div>
                <AddForm inputHandler = {this.props.inputHandler} add = {this.props.addHandler}/>
                <div>
                    <Footer />
                </div>

            </>
        );
    }
}
 
export default Store;