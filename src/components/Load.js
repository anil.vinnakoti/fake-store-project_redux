import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from '../redux'
import Loading from './loading'
import Store from './store'

function Load ({ data, fetchUsers }) {
  useEffect(() => {
    fetchUsers()
  }, [])
  return data.loading 
    ? (
    <Loading />
  ) : data.error ? (
    <h2>{data.error}</h2>
  ) : (
    <div>
      <div>
        <Store
        items = {data.details}
        />
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    data: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchUsers: () => dispatch(fetchUsers())
  }
}

export default connect( mapStateToProps,mapDispatchToProps)(Load)