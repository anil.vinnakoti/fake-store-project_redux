import React, { Component } from 'react';

class Navbar extends Component {

    render() { 
        return (
            <nav id='navbar' className="navbar-expand-lg navbar-light bg-light">
                <div className="navbar container-fluid">
                    <a className="navbar-brand" href="#"><h3>Fake Store</h3></a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="links navbar-nav me-auto mb-2 mb-lg-0">
                            <li className='nav-item'><a href='#'>Home</a></li>
                            <li className='nav-item'><a href='#'>About</a></li>
                            <li className='nav-item'><a href='#'>Contact</a></li>
                        </ul>
                        <form className="search d-flex">
                            <input className="search form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                            <button className="search-button btn btn-outline-success" type="submit">Search</button>
                        </form>
                        
                    </div>
                </div>
            </nav>
        );
    }
}
 
export default Navbar;