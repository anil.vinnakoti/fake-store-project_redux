import React, { Component } from 'react';

class AddButton extends Component {

    render() { 
        return (
            <div>
                <button onClick={this.props.add} type="submit" className="add-button btn">Add Product</button>
            </div>
        );
    }
}
 
export default AddButton;