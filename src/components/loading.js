import React, { Component } from 'react';
 class Loading extends Component {
     state = {  } 
     render() { 
         return (

            <div className="spinner vh-100 d-flex justify-content-center align-items-center">
                <div >
                    <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                Loading...
                </div>
             </div>

         );
     }
 }
  
 export default Loading;