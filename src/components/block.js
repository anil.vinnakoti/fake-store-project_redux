import React, { Component } from 'react';

class Block extends Component {

    render() { 
        const {item} = this.props
        return (
            <div className='card'>
                <div className='image'>
                    <img src={item.image} alt="product icon" />
                </div>
                <div className='content'>
                    <div className='title'>
                        <p>{item.title}</p>
                    </div>

                    <div className='labels'>
                        <p>$ {item.price}</p>
                        <div className='rating'>
                            
                            <p>{item.rating.rate}</p>
                            <img className='star' src='https://icon-library.com/images/favorite-icon/favorite-icon-14.jpg'/>
                            <p>| &nbsp; {item.rating.count}</p>
                        </div>
                    </div>
                </div>
                
            </div>
        )

    }
}
 
export default Block;