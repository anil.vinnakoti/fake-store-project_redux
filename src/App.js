import React from 'react'
import { Provider } from 'react-redux'
import './App.css'
import store from './redux/store'
import Load from './components/Load'

function App () {
  return (
    <Provider store={store}>
      <div className='App'>
        {console.log(store.getState())}
        <Load/>
      </div>
    </Provider>
  )
}

export default App